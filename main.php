<?php
    // Seleção de constantes, principalmente da confirmação dos axiomas, caso seja necessário realizar
    // uma adaptação futura.
    const  Z = 2;
    const A1 = False;
    const A2 = False;
    const A3 = False;
    const A4X = False;
    const A4Y = False;
    const M1 = False;
    const M2 = False;
    const M3 = False;
    const M4X = False;
    const M4Y = False;
    
    function soma($x, $y){
        
        echo "A2 Comutatividade: (x + y) = (y + x).\n";
        echo "(" . $x . " + " . $y . ") = (" . $y ." + " . $x .")\n";
        $aux = $x + $y;
        $aux2 = $y + $x;
        
        if($aux === $aux2){
            echo $aux . " = " . $aux2 . "\n";
            $A2 = (bool)True;
            
        }if( ($x === 0) or ($y === 0)){
            echo "A3 Elemento Neutro: (x + 0) = x  or  (y + 0) = y.\n";
            if( $x == 0){
                echo $x . " + ". $y . " = " . $y . "\n";
                if( ($x + 0) === $x){
                    $A3 = True;
                }
            }else{
                echo $y . " + ". $x . " = " . $x . "\n";
                if( ($y + 0) === $y){
                    $A3 = True;
                    
                }
            }
          
        }
       
        echo "A4 Simétrico x + (-x) = 0 or y + (-y) = 0.\n";
        // transformo o int em string, concateno o caracter - para referenciar um número negativo
        // casting para inteiro novamente. Assim temos o número simétrico.
        if ( ($x > 0) && ($y > 0)){
            $auxStr = (string)$x;
            $auxStr = "-". $auxStr;
            $auxStr = (int)$auxStr;
            echo "Para o valor de x = ". $x . ".\n";
            if( ($x + $auxStr) === 0){
                echo $x . " + (". $auxStr . ") = 0.\n";
                echo ($x + $auxStr) .  " = 0.\n";
                $A4X = True;
            }
            $auxStr = (string)$y;
            $auxStr = "-". $auxStr;
            $auxStr = (int)$auxStr;
            echo "Para o valor de y = ". $y . ".\n";
            if( ($y + $auxStr) === 0){
                echo $y . " + (". $auxStr . ") = 0.\n";
                echo ($y + $auxStr) . " = 0.\n";
                $A4Y = True;
            }
        }else{
            // agora fazendo o mesmo processo para números negativos, porém utilizando a função abs()
            // que retorna o valor absoluto do número 
            echo "Para o valor de x = ". $x . ".\n";
            $aux = abs($x);
            if( ($x + $aux) === 0){
                echo $aux . " + (". $x. ") = 0.\n";
                echo ($aux + $x) .  " = 0.\n";
                $A4X = True;
            }
            echo "Para o valor de y = ". $y . ".\n";
            $aux = abs($y);
            if( ($y + $aux) === 0){
                echo $aux . " + (". $y. ") = 0.\n";
                echo ($aux + $y) .  " = 0.\n";
                $A4Y = True;
            }   
        }
        return 0;
    }
    
    function produto($x, $y){
        
        echo "M2 Comutatividade: (x * y) = (y * x).\n";
        echo "(" . $x . " * " . $y . ") = (" . $y ."*" . $x .")\n";
        
        if(($x * $y) === ($y * $x)){
            echo ( $x * $y) . " = " . ( $y * $x) . "\n";
            $M2 = (bool)True;
            
        }
        if( ($x == 0 ) or ( $y == 0 )){
            
            echo "M3 - Elemento Neutro, não pode ser satisfeita. Pois o valor de x ou de y é = 0.\n";
        }
        
        echo "M3 Elemento Neutro: (x * 1) = x  or  (y * 1) = y.\n"; 
        echo "(" . $x . " * 1)" . " = " . $x . "\n";
        echo "(" . $y . " * 1)" . " = " . $y . "\n";
        
        echo "M4 Inverso Multiplicativo: x * (x ^ (-1)) = x.\n";
        
        if ( $x == 0){
            
            echo "X Não satisfaz a propriedade de Inverso Multiplicativo por ser igual a 0.\n";
            
        }
        if( $y == 0){
            
            echo "Y Não satisfaz a propriedade de Inverso Multiplicativo por ser igual a 0.\n";
        }
        if( ( $x * pow( $x, -1)) == 1){
            
            echo $x . "*( " . $x . " ^ (-1)) = 1.\n";
            echo ($x * pow( $x, -1)) . " = 1.\n"; 
            $M4X = (boolean)True;
        }
        if( ( $y * pow( $y, -1)) == 1){
            echo $y . "*( " . $y . " ^ (-1)) = 1.\n";
            echo ($y * pow( $y, -1)) . " = 1.\n"; 
            $M4Y = (boolean)True;
            
        }
     
        return 0;
    }

    function veriSoma($x, $y, $Z){
 
        echo "A1 Associatividade: (x + y) + z = x + (y + z).\n";
        echo "(" . $x . " + " . $y . ") + " . Z . " = " . $x ." + (" . $y . " + " . Z . ")\n";
        
        $aux = ($x + $y) + Z ;
        $aux2 = $x + ($y + Z);
        
        
        if ($aux === $aux2){
            
            echo $aux . " = " . $aux2 . "\n";
            $A1 = (bool)True;
        }
        
        return 0;
    }

    function veriProduto($x, $y, $Z){
        
        echo "M1 Associatividade: (x * y) * z = x * (y * z).\n";
        echo "(" . $x . " * " . $y . ") * " . Z . " = " . $x ." * (" . $y . " * " . Z . ")\n";
        
        if ((( $x * $y) * Z ) === ($x * ( $y * Z))){
            echo ( $x * $y) * Z  . " = " . $x * ( $y * Z) . "\n";
            $M1 = (bool)True;
        }
        return 0;
    }
    
    echo "Questão 1 \n";

    $x = (int)readline("Digite um valor para x: ");
    $y = (int)readline("Digite um valor para y: ");
              
    echo "\n***Axiomas da Adição***\n";
    veriSoma($x, $y, Z);
    soma($x, $y);
    
    echo "***Axiomas do Produto***\n";
    veriProduto($x, $y, Z);
    produto($x, $y);
    
    
?>

